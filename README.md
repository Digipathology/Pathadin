# Pathadin

Pathadin is a tool for quick and simple quantitative pathology. Main developers @dimathe47, @Digipathology (concept)

The concept of Pathadin differs from other desktop WSI alternatives: in contrast to unitary solutions, which require rather powerful machines, it is a combinatory toolset, where each part can be separately updated and modified; this also includes the opportunity of the A.I. training without strict linkage to the local computer, as described further. Such an approach gives a better understanding of functional elements of modern computer-assisted image analysis, makes the program more flexible in the context of modification, updating and testing different models, and allows users to run digital pathology on the basic hardware.

Among Java solutions, Pathadin is entirely Python 3.6 software built with PyQT5 GUI. Pathadin is based on the openslide library. The central analytic libraries include Keras, Skimage/ Sklearn, and Histomics TK.

Pathadin project provides:
- Main graphical user interface (GUI)-application
- Tool for slicing digital slides – a convenient feature for dataset generation for machine learning or usage in alternative software
- Simply reproducable example of U-net model training.
- Supports all the Openslide formats for Whole Slide Imaging (WSI), as the main image formats.

The U-net model training can be performed separately from GUI due to training been computationally heavy and excessively dynamic, thus irrational to be enclosed by GUI.


##### Minimum system requirements for adequate experience for provided binaries include
* Monitor resolution 1280 × 720
* Operation system Windows 7 SP1 or newer
* CPU Intel DualCore, RAM 4 GB

##### Getting started
* Pathadin [Wiki](https://gitlab.com/Digipathology/Pathadin/-/wikis/home) on GitLab
* https://doi.org/10.1016/j.acthis.2020.151619

##### **Additional infrotmation for the manuscript: "How to create an applied multiclass machine learning model in histology: a prostate based example"**
- Dataset generation ([Google Colab](https://colab.research.google.com/drive/1gY4rloBJj1xqR325AJDM_8CC6hDrqsl7?usp=sharing)) by @alegvo
- Model training ([Google Colab](https://colab.research.google.com/drive/1NMo3vjvpE_CGbXZLGSTa3pOwKYsAtCdX?usp=sharing)) by @alegvo
- 5 class segmentation trained [model](https://www.pathadin.eu/pathadin/5class.h5)
- [Example of ML validation](https://pathadin.eu/pathadin/validation.pdf)

##### **Addtional information for the manuscript:"[Pathadin – The essential set of tools to start with whole slide analysis](https://doi.org/10.1016/j.acthis.2020.151619)"**
* Dataset generation ([Google Colab](https://colab.research.google.com/drive/1ZWul3MWKwKVNJXz6S1AsMeDifrSWpkla?usp=sharing))
* [Dataset](https://www.pathadin.eu/pathadin/pathadin_examples/segmentation_example/data/slice_example_patches.zip)
* [Screenshot of dataset archive structure](https://www.pathadin.eu/pathadin/pathadin_examples/segmentation_example/slice_example_patches_screen.png)
* Model training ([Google Colab](https://colab.research.google.com/drive/19uCd12Ru9Gu3Mk9wOgTPmJ6ItKyy1pWl?usp=sharing))
* [Trained model - h5 keras model trained in segmentation_example](https://www.pathadin.eu/pathadin/Stroma&Glands.h5)
* [Screenshot of trained model usage as filter in Pathadin main app](https://www.pathadin.eu/pathadin/Stroma&Glands.h5)

**[NB! Adding atributes for annotations (for dataset generation)](https://gitlab.com/Digipathology/Pathadin/-/wikis/Annotations)**